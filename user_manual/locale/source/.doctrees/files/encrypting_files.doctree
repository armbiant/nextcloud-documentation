���l      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�h �section���)��}�(hhh]�(h �title���)��}�(h�-Encrypting your Nextcloud files on the server�h]�h �Text����-Encrypting your Nextcloud files on the server�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�8/github/workspace/user_manual/files/encrypting_files.rst�hK�uid�� 19049d5239614febb56c3f0601127705�ubh �	paragraph���)��}�(hX  Nextcloud includes a server side Encryption app, and when it is enabled by
your Nextcloud administrator all of your Nextcloud data files are automatically
encrypted on the server.
Encryption is server-wide, so when it is enabled you cannot choose to keep your
files unencrypted. You don't have to do anything special, as it uses your
Nextcloud login as the password for your unique private encryption key. Just log
out and in and manage and share your files as you normally do, and you can
still change your password whenever you want.�h]�hX  Nextcloud includes a server side Encryption app, and when it is enabled by
your Nextcloud administrator all of your Nextcloud data files are automatically
encrypted on the server.
Encryption is server-wide, so when it is enabled you cannot choose to keep your
files unencrypted. You don’t have to do anything special, as it uses your
Nextcloud login as the password for your unique private encryption key. Just log
out and in and manage and share your files as you normally do, and you can
still change your password whenever you want.�����}�(hh1hh/hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h-hh*hKhhhhh+� 525c5607383a4b0a8230a9e58bc13b75�ubh.)��}�(hX�  Its main purpose is to encrypt files on remote storage services that are
connected to your Nextcloud server. This is an
easy and seamless way to protect your files on remote storage. You can share
your remote files through Nextcloud in the usual way, however you cannot share
your encrypted files directly from the remote service you are using, because
the encryption keys are stored on your Nextcloud server, and are never exposed
to outside service providers.�h]�hX�  Its main purpose is to encrypt files on remote storage services that are
connected to your Nextcloud server. This is an
easy and seamless way to protect your files on remote storage. You can share
your remote files through Nextcloud in the usual way, however you cannot share
your encrypted files directly from the remote service you are using, because
the encryption keys are stored on your Nextcloud server, and are never exposed
to outside service providers.�����}�(hh@hh>hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h-hh*hKhhhhh+� 36508e4a66b0420686f84e912dfc3f1c�ubh.)��}�(hX�  If your Nextcloud server is not connected to any remote storage services, then
it is better to use some other form of encryption such as file-level or whole
disk encryption. Because the keys are kept on your Nextcloud server, it is
possible for your Nextcloud administrator to snoop in your files, and if the server is
compromised the intruder may get access to your files. (Read
`Encryption in Nextcloud <https://nextcloud.com/blog/encryption-in-nextcloud/>`_
to learn more.)�h]�(hX|  If your Nextcloud server is not connected to any remote storage services, then
it is better to use some other form of encryption such as file-level or whole
disk encryption. Because the keys are kept on your Nextcloud server, it is
possible for your Nextcloud administrator to snoop in your files, and if the server is
compromised the intruder may get access to your files. (Read
�����}�(hX|  If your Nextcloud server is not connected to any remote storage services, then
it is better to use some other form of encryption such as file-level or whole
disk encryption. Because the keys are kept on your Nextcloud server, it is
possible for your Nextcloud administrator to snoop in your files, and if the server is
compromised the intruder may get access to your files. (Read
�hhMhhhNhNubh �	reference���)��}�(h�P`Encryption in Nextcloud <https://nextcloud.com/blog/encryption-in-nextcloud/>`_�h]�h�Encryption in Nextcloud�����}�(h�Encryption in Nextcloud�hhXubah}�(h]�h!]�h#]�h%]�h']��name��Encryption in Nextcloud��refuri��3https://nextcloud.com/blog/encryption-in-nextcloud/�uh)hVhhMubh �target���)��}�(h�6 <https://nextcloud.com/blog/encryption-in-nextcloud/>�h]�h}�(h]��encryption-in-nextcloud�ah!]�h#]��encryption in nextcloud�ah%]�h']��refuri�hjuh)hk�
referenced�KhhMubh�
to learn more.)�����}�(h�
to learn more.)�hhMhhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h-hh*hKhhhhh+� b335918f50724726bd7120109809b026�ubh
)��}�(hhh]�(h)��}�(h�Encryption FAQ�h]�h�Encryption FAQ�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhh�hhhh*hKh+� 684b3c09633c46c89d897aae291862d7�ubh
)��}�(hhh]�(h)��}�(h�How can encryption be disabled?�h]�h�How can encryption be disabled?�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhh�hhhh*hK"h+� b82200640a25454dbcea38e11e0ecc29�ubh.)��}�(h��The only way to disable encryption is to run the `"decrypt all"
<https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/occ_command.html#encryption-label>`_
script, which decrypts all files and disables encryption.�h]�(h�1The only way to disable encryption is to run the �����}�(h�1The only way to disable encryption is to run the �hh�hhhNhNubhW)��}�(h�`"decrypt all"
<https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/occ_command.html#encryption-label>`_�h]�h�“decrypt all”�����}�(h�"decrypt all"�hh�ubah}�(h]�h!]�h#]�h%]�h']��name��"decrypt all"�hi�lhttps://docs.nextcloud.com/server/latest/admin_manual/configuration_server/occ_command.html#encryption-label�uh)hVhh�ubhl)��}�(h�o
<https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/occ_command.html#encryption-label>�h]�h}�(h]��decrypt-all�ah!]�h#]��"decrypt all"�ah%]�h']��refuri�h�uh)hkhzKhh�ubh�:
script, which decrypts all files and disables encryption.�����}�(h�:
script, which decrypts all files and disables encryption.�hh�hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h-hh*hK$hh�hhh+� 4c770f8abdf942f4b6f02c4ddb4c1f6f�ubh �comment���)��}�(h�7TODO ON RELEASE: Update version number above on release�h]�h�7TODO ON RELEASE: Update version number above on release�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']��	xml:space��preserve�uh)h�hh�hhhh*hK)ubeh}�(h]��how-can-encryption-be-disabled�ah!]�h#]��how can encryption be disabled?�ah%]�h']�uh)h	hh�hhhh*hK"ubh
)��}�(hhh]�(h)��}�(h�;Is it possible to disable encryption with the recovery key?�h]�h�;Is it possible to disable encryption with the recovery key?�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhh�hhhh*hK+h+� 7581db351c2c4e3d826bb120e971f31d�ubh.)��}�(hXa  Yes, *if* every user uses the `file recovery key
<https://docs.nextcloud.com/server/latest/admin_manual/configuration_files/encryption_configuration.html#enabling-users-file-recovery-keys>`_, `"decrypt all"
<https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/occ_command.html#encryption-label>`_ will use it to decrypt all files.�h]�(h�Yes, �����}�(h�Yes, �hj  hhhNhNubh �emphasis���)��}�(h�*if*�h]�h�if�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']�uh)j  hj  ubh� every user uses the �����}�(h� every user uses the �hj  hhhNhNubhW)��}�(h��`file recovery key
<https://docs.nextcloud.com/server/latest/admin_manual/configuration_files/encryption_configuration.html#enabling-users-file-recovery-keys>`_�h]�h�file recovery key�����}�(h�file recovery key�hj)  ubah}�(h]�h!]�h#]�h%]�h']��name��file recovery key�hi��https://docs.nextcloud.com/server/latest/admin_manual/configuration_files/encryption_configuration.html#enabling-users-file-recovery-keys�uh)hVhj  ubhl)��}�(h��
<https://docs.nextcloud.com/server/latest/admin_manual/configuration_files/encryption_configuration.html#enabling-users-file-recovery-keys>�h]�h}�(h]��file-recovery-key�ah!]�h#]��file recovery key�ah%]�h']��refuri�j:  uh)hkhzKhj  ubh�, �����}�(h�, �hj  hhhNhNubhW)��}�(h�`"decrypt all"
<https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/occ_command.html#encryption-label>`_�h]�h�“decrypt all”�����}�(h�"decrypt all"�hjM  ubah}�(h]�h!]�h#]�h%]�h']��name��"decrypt all"�hi�lhttps://docs.nextcloud.com/server/latest/admin_manual/configuration_server/occ_command.html#encryption-label�uh)hVhj  ubhl)��}�(h�o
<https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/occ_command.html#encryption-label>�h]�h}�(h]��id1�ah!]�h#]�h%]��"decrypt all"�ah']��refuri�j^  uh)hkhzKhj  ubh�" will use it to decrypt all files.�����}�(h�" will use it to decrypt all files.�hj  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h-hh*hK-hh�hhh+� 539d8b6495bc44089f0c523f1c1592cd�ubh�)��}�(h�7TODO ON RELEASE: Update version number above on release�h]�h�7TODO ON RELEASE: Update version number above on release�����}�(hhhjx  ubah}�(h]�h!]�h#]�h%]�h']�h�h�uh)h�hh�hhhh*hK2ubeh}�(h]��:is-it-possible-to-disable-encryption-with-the-recovery-key�ah!]�h#]��;is it possible to disable encryption with the recovery key?�ah%]�h']�uh)h	hh�hhhh*hK+ubh
)��}�(hhh]�(h)��}�(h�7Can encryption be disabled without the user's password?�h]�h�9Can encryption be disabled without the user’s password?�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hK4h+� 4b2f1db41c1e44dc95ee7e62155ea646�ubh.)��}�(hXZ  If you don't have the users password or `file recovery key
<https://docs.nextcloud.com/server/latest/admin_manual/configuration_files/encryption_configuration.html#enabling-users-file-recovery-keys>`_,
then there is no way to decrypt all files. What's more, running it on login
would be dangerous, because you would most likely run into timeouts.�h]�(h�*If you don’t have the users password or �����}�(h�(If you don't have the users password or �hj�  hhhNhNubhW)��}�(h��`file recovery key
<https://docs.nextcloud.com/server/latest/admin_manual/configuration_files/encryption_configuration.html#enabling-users-file-recovery-keys>`_�h]�h�file recovery key�����}�(h�file recovery key�hj�  ubah}�(h]�h!]�h#]�h%]�h']��name��file recovery key�hi��https://docs.nextcloud.com/server/latest/admin_manual/configuration_files/encryption_configuration.html#enabling-users-file-recovery-keys�uh)hVhj�  ubhl)��}�(h��
<https://docs.nextcloud.com/server/latest/admin_manual/configuration_files/encryption_configuration.html#enabling-users-file-recovery-keys>�h]�h}�(h]��id2�ah!]�h#]�h%]��file recovery key�ah']��refuri�j�  uh)hkhzKhj�  ubh��,
then there is no way to decrypt all files. What’s more, running it on login
would be dangerous, because you would most likely run into timeouts.�����}�(h��,
then there is no way to decrypt all files. What's more, running it on login
would be dangerous, because you would most likely run into timeouts.�hj�  hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h-hh*hK6hj�  hhh+� f3ad3e37741d4f758f37ac7494ee091f�ubh�)��}�(h�7TODO ON RELEASE: Update version number above on release�h]�h�7TODO ON RELEASE: Update version number above on release�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�h�h�uh)h�hj�  hhhh*hK<ubeh}�(h]��6can-encryption-be-disabled-without-the-user-s-password�ah!]�h#]��7can encryption be disabled without the user's password?�ah%]�h']�uh)h	hh�hhhh*hK4ubh
)��}�(hhh]�(h)��}�(h�FIs it planned to move this to the next user login or a background job?�h]�h�FIs it planned to move this to the next user login or a background job?�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hK>h+� 1d256a7ce19b49089a7b4e6dc9807858�ubh.)��}�(h��If we did that, then we would need to store your login password in the database.
This could be seen as a security issue, so nothing like that is planned.�h]�h��If we did that, then we would need to store your login password in the database.
This could be seen as a security issue, so nothing like that is planned.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h-hh*hK@hj�  hhh+� 3ced30b907b34924a0516464d11c4897�ubeh}�(h]��Eis-it-planned-to-move-this-to-the-next-user-login-or-a-background-job�ah!]�h#]��Fis it planned to move this to the next user login or a background job?�ah%]�h']�uh)h	hh�hhhh*hK>ubh
)��}�(hhh]�(h)��}�(h�0Is group Sharing possible with the recovery key?�h]�h�0Is group Sharing possible with the recovery key?�����}�(hj  hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj  hhhh*hKDh+� 65a706be277945a09dfdf81c19402601�ubh.)��}�(h�gIf you mean adding users to groups and make it magically work? No. This only
works with the master key.�h]�h�gIf you mean adding users to groups and make it magically work? No. This only
works with the master key.�����}�(hj'  hj%  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h-hh*hKFhj  hhh+� bab63fe0ee724483bda63c5d055446d7�ubeh}�(h]��/is-group-sharing-possible-with-the-recovery-key�ah!]�h#]��0is group sharing possible with the recovery key?�ah%]�h']�uh)h	hh�hhhh*hKDubeh}�(h]��encryption-faq�ah!]�h#]��encryption faq�ah%]�h']�uh)h	hhhhhh*hKubh
)��}�(hhh]�(h)��}�(h�Using encryption�h]�h�Using encryption�����}�(hjI  hjG  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhjD  hhhh*hKJh+� 97065047f00a403093a03752f2ae49ac�ubh.)��}�(h�aNextcloud encryption is pretty much set it and forget it, but you have a few
options you can use.�h]�h�aNextcloud encryption is pretty much set it and forget it, but you have a few
options you can use.�����}�(hjX  hjV  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h-hh*hKLhjD  hhh+� 0a017ed755db42128fc7113f714bf489�ubh.)��}�(hX<  When your Nextcloud administrator enables encryption for the first time, you must log
out and then log back in to create your encryption keys and encrypt your files.
When encryption has been enabled on your Nextcloud server you will see a yellow
banner on your Files page warning you to log out and then log back in:�h]�hX<  When your Nextcloud administrator enables encryption for the first time, you must log
out and then log back in to create your encryption keys and encrypt your files.
When encryption has been enabled on your Nextcloud server you will see a yellow
banner on your Files page warning you to log out and then log back in:�����}�(hjg  hje  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h-hh*hKOhjD  hhh+� 36e6ef5a595b49c19a5c0716f48e786e�ubh �figure���)��}�(hhh]�h �image���)��}�(h�&.. figure:: ../images/encryption1.png
�h]�h}�(h]�h!]�h#]�h%]�h']��uri��files/../images/encryption1.png��
candidates�}��*�j�  suh)jy  hjv  hh*hKUubah}�(h]�h!]�h#]�h%]�h']��align��default�uh)jt  hjD  hhhh*hKUubh.)��}�(h��When you log back in it takes a few minutes to work, depending on how many
files you have, and then you are returned to your default Nextcloud page.�h]�h��When you log back in it takes a few minutes to work, depending on how many
files you have, and then you are returned to your default Nextcloud page.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h-hh*hKVhjD  hhh+� 79dfc5686aaa4e82850a035293f166bc�ubju  )��}�(hhh]�jz  )��}�(h�'.. figure:: ../images/encryption2.png

�h]�h}�(h]�h!]�h#]�h%]�h']��uri��files/../images/encryption2.png�j�  }�j�  j�  suh)jy  hj�  hh*hK[ubah}�(h]�h!]�h#]�h%]�h']�j�  j�  uh)jt  hjD  hhhh*hK[ubh �note���)��}�(h��You must never lose your Nextcloud password, because you will lose
access to your files. Though there is an optional recovery option that your
Nextcloud administrator may enable; see the Recovery Key Password section
(below) to learn about this.�h]�h.)��}�(h��You must never lose your Nextcloud password, because you will lose
access to your files. Though there is an optional recovery option that your
Nextcloud administrator may enable; see the Recovery Key Password section
(below) to learn about this.�h]�h��You must never lose your Nextcloud password, because you will lose
access to your files. Though there is an optional recovery option that your
Nextcloud administrator may enable; see the Recovery Key Password section
(below) to learn about this.�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h-hh*hK\hj�  h+� 90d997ab96164d60bb1292511a6e6284�ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hjD  hhhh*hNubeh}�(h]��using-encryption�ah!]�h#]��using encryption�ah%]�h']�uh)h	hhhhhh*hKJubh
)��}�(hhh]�(h)��}�(h�Sharing encrypted files�h]�h�Sharing encrypted files�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hKbh+� 7f18f363525c435f9696a3d7801710e3�ubh.)��}�(hX�  Only users who have private encryption keys have access to shared encrypted
files and folders. Users who have not yet created their private encryption keys
will not have access to encrypted shared files; they will see folders and
filenames, but will not be able to open or download the files. They will see a
yellow warning banner that says "Encryption App is enabled but your keys are not
initialized, please log-out and log-in again."�h]�hX�  Only users who have private encryption keys have access to shared encrypted
files and folders. Users who have not yet created their private encryption keys
will not have access to encrypted shared files; they will see folders and
filenames, but will not be able to open or download the files. They will see a
yellow warning banner that says “Encryption App is enabled but your keys are not
initialized, please log-out and log-in again.”�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h-hh*hKdhj�  hhh+� 4ac873401f844d13b3ef777509f40f05�ubh.)��}�(hX�  Share owners may need to re-share files after encryption is enabled; users
trying to access the share will see a message advising them to ask the share
owner to re-share the file with them. For individual shares, un-share and
re-share the file. For group shares, share with any individuals who can't access
the share. This updates the encryption, and then the share owner can remove the
individual shares.�h]�hX�  Share owners may need to re-share files after encryption is enabled; users
trying to access the share will see a message advising them to ask the share
owner to re-share the file with them. For individual shares, un-share and
re-share the file. For group shares, share with any individuals who can’t access
the share. This updates the encryption, and then the share owner can remove the
individual shares.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h-hh*hKkhj�  hhh+� 405d30730c1949c7805e0e1571c6b6e9�ubh
)��}�(hhh]�(h)��}�(h�Recovery key password�h]�h�Recovery key password�����}�(hj  hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj
  hhhh*hKsh+� b7a445e0c3a247aea954e8db08ca3972�ubh.)��}�(hX�  If your Nextcloud administrator has enabled the recovery key feature, you can
choose to use this feature for your account. If you enable "Password recovery"
the administrator can read your data with a special password. This feature
enables the administrator to recover your files in the event you lose your
Nextcloud password. If the recovery key is not enabled, then there is no way to
restore your files if you lose your login password.�h]�hX�  If your Nextcloud administrator has enabled the recovery key feature, you can
choose to use this feature for your account. If you enable “Password recovery”
the administrator can read your data with a special password. This feature
enables the administrator to recover your files in the event you lose your
Nextcloud password. If the recovery key is not enabled, then there is no way to
restore your files if you lose your login password.�����}�(hj  hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h-hh*hKuhj
  hhh+� a8508f9e869f44c2832ebeb87248829b�ubju  )��}�(hhh]�jz  )��}�(h�&.. figure:: ../images/encryption3.png
�h]�h}�(h]�h!]�h#]�h%]�h']��uri��files/../images/encryption3.png�j�  }�j�  j9  suh)jy  hj+  hh*hK}ubah}�(h]�h!]�h#]�h%]�h']�j�  j�  uh)jt  hj
  hhhh*hK}ubeh}�(h]��recovery-key-password�ah!]�h#]��recovery key password�ah%]�h']�uh)h	hj�  hhhh*hKsubeh}�(h]��sharing-encrypted-files�ah!]�h#]��sharing encrypted files�ah%]�h']�uh)h	hhhhhh*hKbubh
)��}�(hhh]�(h)��}�(h�Files not encrypted�h]�h�Files not encrypted�����}�(hjV  hjT  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhjQ  hhhh*hKh+� 6dc18d83c3d3477389cd014276a05be5�ubh.)��}�(h�vOnly the data in your files is encrypted, and not the filenames or folder
structures. These files are never encrypted:�h]�h�vOnly the data in your files is encrypted, and not the filenames or folder
structures. These files are never encrypted:�����}�(hje  hjc  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h-hh*hK�hjQ  hhh+� 5d03b8ed0a654dbc97436bbc6fbc9e7a�ubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�Old files in the trash bin.�h]�h.)��}�(hj{  h]�h�Old files in the trash bin.�����}�(hj{  hj}  ubah}�(h]�h!]�h#]�h%]�h']�uh)h-hh*hK�hjy  h+� 89753a3ebdef4ea48a73144952bed53e�ubah}�(h]�h!]�h#]�h%]�h']�uh)jw  hjt  hhhh*hNubjx  )��}�(h�&Image thumbnails from the Gallery app.�h]�h.)��}�(hj�  h]�h�&Image thumbnails from the Gallery app.�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h-hh*hK�hj�  h+� 69b08e94f1db49cbb6561e11c31c0776�ubah}�(h]�h!]�h#]�h%]�h']�uh)jw  hjt  hhhh*hNubjx  )��}�(h�Previews from the Files app.�h]�h.)��}�(hj�  h]�h�Previews from the Files app.�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h-hh*hK�hj�  h+� 4e63b2d8bc5f4e9e926bde1ff667e94e�ubah}�(h]�h!]�h#]�h%]�h']�uh)jw  hjt  hhhh*hNubjx  )��}�(h�/The search index from the full text search app.�h]�h.)��}�(hj�  h]�h�/The search index from the full text search app.�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h-hh*hK�hj�  h+� 95b113ac0fc7445883c62a7b1e68571c�ubah}�(h]�h!]�h#]�h%]�h']�uh)jw  hjt  hhhh*hNubjx  )��}�(h�Third-party app data
�h]�h.)��}�(h�Third-party app data�h]�h�Third-party app data�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h-hh*hK�hj�  h+� 6be2831c141648c6aeae8d13792f8853�ubah}�(h]�h!]�h#]�h%]�h']�uh)jw  hjt  hhhh*hNubeh}�(h]�h!]�h#]�h%]�h']��bullet��-�uh)jr  hh*hK�hjQ  hhubh.)��}�(h��Only those files that are shared with third-party storage providers can
be encrypted, the rest of the files may not be encrypted.�h]�h��Only those files that are shared with third-party storage providers can
be encrypted, the rest of the files may not be encrypted.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h-hh*hK�hjQ  hhh+� 08c94b61cab242d7a8f46fbd3f1b3727�ubh
)��}�(hhh]�(h)��}�(h�Change private key password�h]�h�Change private key password�����}�(hj  hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj	  hhhh*hK�h+� 1137d04ce82f41d19cebc1f05558be0d�ubh.)��}�(hX�  This option is only available if the encryption password has not been changed by
the administrator, but only the log-in password. This can occur if your Nextcloud
provider uses an external user back-end (for example, LDAP) and changed your
login password using that back-end configuration. In this case, you can set
your encryption password to your new login password by providing your old and
new login password. The Encryption app works only if your login password and
your encryption password are identical.�h]�hX�  This option is only available if the encryption password has not been changed by
the administrator, but only the log-in password. This can occur if your Nextcloud
provider uses an external user back-end (for example, LDAP) and changed your
login password using that back-end configuration. In this case, you can set
your encryption password to your new login password by providing your old and
new login password. The Encryption app works only if your login password and
your encryption password are identical.�����}�(hj  hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h-hh*hK�hj	  hhh+� a592356d6815454e94c9f3592c461a70�ubeh}�(h]��change-private-key-password�ah!]�h#]��change private key password�ah%]�h']�uh)h	hjQ  hhhh*hK�ubeh}�(h]��files-not-encrypted�ah!]�h#]��files not encrypted�ah%]�h']�uh)h	hhhhhh*hKubeh}�(h]��-encrypting-your-nextcloud-files-on-the-server�ah!]�h#]��-encrypting your nextcloud files on the server�ah%]�h']�uh)h	hhhhhh*hKubah}�(h]�h!]�h#]�h%]�h']��source�h*uh)h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�je  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h*�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��version�h �substitution_definition���)��}�(h�.. |version| replace:: latest�h]�h�latest�����}�(h�latest�hj�  ubah}�(h]�h!]�h#]�j�  ah%]�h']�uh)j�  h�<rst_epilog>�hKhj	  hhubs�substitution_names�}��version�j�  s�refnames�}��refids�}��nameids�}�(j?  j<  hvhsjA  j>  h�h�h�h�j�  j�  jD  jA  j�  j�  j  j  j9  j6  j�  j�  jN  jK  jF  jC  j7  j4  j/  j,  u�	nametypes�}�(j?  Nhv�jA  Nh�Nhψj�  NjD  �j�  Nj  Nj9  Nj�  NjN  NjF  Nj7  Nj/  Nuh}�(j<  hhshmj>  h�h�h�h�h�j�  h�jA  j;  je  j_  j�  j�  j�  j�  j  j�  j6  j  j�  jD  jK  j�  jC  j
  j4  jQ  j,  j	  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}�js  Ks��R��parse_messages�]�(h �system_message���)��}�(hhh]�h.)��}�(h�0Duplicate explicit target name: ""decrypt all"".�h]�h�8Duplicate explicit target name: “”decrypt all””.�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h-hj�  ubah}�(h]�h!]�h#]�h%]�h']�je  a�level�K�type��INFO��source�h*�line�Kuh)j�  hh�hhhh*hK/ubj�  )��}�(hhh]�h.)��}�(h�4Duplicate explicit target name: "file recovery key".�h]�h�8Duplicate explicit target name: “file recovery key”.�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h-hj�  ubah}�(h]�h!]�h#]�h%]�h']�j�  a�level�K�type�j�  �source�h*�line�Kuh)j�  hj�  hhhh*hK9ube�transform_messages�]��transformer�N�
decoration�Nhhub.